﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SpeechLib;

namespace 语音
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string strVodeo; 
        private void button1_Click(object sender, EventArgs e)
        {
            strVodeo = textBox1.Text;
            TextToSpeech(strVodeo);
        }
        public void TextToSpeech(string TextVoice)
        {
            SpeechVoiceSpeakFlags flag = SpeechVoiceSpeakFlags.SVSFlagsAsync;
            SpVoice voice = new SpVoice();
            string voice_text = TextVoice;
            voice.Voice = voice.GetVoices(string.Empty, string.Empty).Item(0);
            voice.Speak(voice_text, flag);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = "福州千里马欢迎您！";
        }
    }
}
